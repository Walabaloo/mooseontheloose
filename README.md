#Moose Java Project (Moose on the Loose)

To run:
* Download zip file (or clone the repository)
* Open with IntelliJ
* Set ```src``` as source root by ```Right Click > Mark Directory As > Sources Root```
* Set ```resources``` as resource root by ```Right Click > Mark Directory As > Resources Root```
* Create an ```out``` directory
* Set ```out``` as output directory by ```File > Project Structure... > Project > Project Compiler Output```
* Run the ```MooseOnTheLoose``` class in the ```Game``` package