package Actor;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;

import Stage.Stage;
import Stage.ResourceLoader;

/**
 * Will allow the player to navigate through the menus with the arrow keys
 */
public class Selector extends Actor implements KeyboardControllable {
    // Number of buffer pixels
    private static int bufferX = 10;
    private static int bufferY = 0;

    private List<Selection> selections;
    private Selection selected;
    private int selectedIndex;

    /**
     * Create a new selector
     * @param canvas The canvas to draw to
     * @param selections The selections to select from
     * @param defaultSelectionIndex The default selection
     */
    public Selector(Stage canvas, List<Selection> selections, int defaultSelectionIndex) {
        super(canvas);
        sprites = new String[]{"MooseSelector_1.png", "MooseSelector_2.png"};

        this.selections = selections;
        this.selectedIndex = defaultSelectionIndex;
        changeSelection(selectedIndex);

        frameSpeed = 30;
        actorSpeed = 50;
        width = 50;
        height = 50;
    }

    @Override
    public void paint(Graphics g) {
        try {
            g.drawImage(ResourceLoader.getInstance().getSprite(sprites[frame]), posX - width, posY - height, width, height, stage);
        }
        catch (NullPointerException e) {
            System.err.println(getClass().toString());
            e.printStackTrace();
        }
    }

    /**
     * Move the selector to a different selection
     * @param selectedIndex The index on the new selection
     */
    public void changeSelection(int selectedIndex) {
        this.selectedIndex = selectedIndex;
        this.selected = selections.get(selectedIndex);
        updatePosition();
    }

    /**
     * Update the position on the screen to paint the selector
     */
    public void updatePosition() {
        posX = selected.getxPos() - bufferX;
        posY = selected.getyPos() - bufferY;
    }

    /**
     * Move to the next selection on the screen
     */
    public void nextSelection() {
        int nextIndex = (selectedIndex + 1 < selections.size() ? selectedIndex + 1 : 0);
        changeSelection(nextIndex);
    }

    /**
     * Move to the previous selection on the screen
     */
    public void previousSelection() {
        int previousIndex = (selectedIndex - 1 >= 0 ? selectedIndex - 1 : selections.size() - 1);
        changeSelection(previousIndex);
    }

    /**
     * Handle key press event
     * @param e The key event
     */
    @Override
    public void triggerKeyPress(KeyEvent e) {}

    /**
     * Handle key released event
     * @param e The key event
     */
    @Override
    public void triggerKeyRelease(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_DOWN:
                nextSelection();
                playSound("SelectorMove.wav");
                break;
            case KeyEvent.VK_UP:
                previousSelection();
                playSound("SelectorMove.wav");
                break;
            case KeyEvent.VK_ENTER:
            case KeyEvent.VK_SPACE:
                selected.select();
                playSound("SelectorSelect.wav");
                break;
            default:
                break;
        }
    }
}
