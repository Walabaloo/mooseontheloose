package Actor;

import Stage.Stage;

public class BoxTruck extends Vehicle {
    public static int MAX_SPEED = 3;

    public BoxTruck(Stage canvas, int speed, Direction direction) {
        super(canvas, speed, direction);

        width = 64;
        height = 150;

        switch (direction) {
            case Up:
                sprites = new String[]{"CarTruckUp_4.png"};
                break;
            case Down:
                sprites = new String[]{"CarTruckDown_4.png"};
                break;
        }
    }
}
