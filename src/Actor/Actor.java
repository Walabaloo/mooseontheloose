package Actor;

import Stage.ResourceLoader;
import Stage.Stage;
import java.awt.Graphics;
import java.awt.Rectangle;


abstract public class Actor {

	private static final int POINT_VALUE = 0;
	protected int vx; 
	protected int vy;
	protected int posX;
	protected int posY;
	protected int height;
	protected int width;
	protected int frame;
	protected int frameSpeed;
	protected int actorSpeed;
	protected int time;
	private boolean markedForRemoval = false;
	protected String[] sprites = null; 
	protected Stage stage = null;

    /**
     * Create new actor
     * @param canvas Stage for the actor to interact with
     */
	public Actor(Stage canvas) {
		this.stage = canvas;
		frame = 0;
		frameSpeed = 1;
		actorSpeed = 10;
		time = 0;
	}

    /**
     * Update the state of the actor
     */
	public void update() {
		updateSpriteAnimation();
	}

    /**
     * Cycle through the actor's frame(s)
     */
	private void updateSpriteAnimation() {
		time++;
		if (time % frameSpeed == 0) {
			time = 0;
			frame = (frame + 1) % sprites.length;
		}
	}

    /**
     * Play a sound resource
     * @param name Name of the sound resource
     */
	public void playSound(final String name) {
	    stage.getContainer().playSound(name);
	}

//	public void paint(Graphics g) {
//		g.drawImage(ResourceLoader.getInstance().getSprite(sprites[frame]), posX, posY, stage);
//	}

    /**
     * Paint the actor graphics to the screen
     * @param g The canvas' graphics object
     */
	public void paint(Graphics g) {
        try {
            g.drawImage(ResourceLoader.getInstance().getSprite(sprites[frame]), posX, posY, width, height, stage);
        }
        catch (NullPointerException e) {
            System.err.println(getClass().toString());
            e.printStackTrace();
        }
    }

    /**
     * Set the X coordinate of the actor
     * @param posX New X coordinate
     */
	public void setX(int posX) {
		this.posX = posX;
	}

    /**
     * Set the Y coordinate of the actor
     * @param posY New Y coordinate
     */
	public void setY(int posY) {
		this.posY = posY;
	}

    /**
     * Get the current X coordinate of the actor
     * @return The current X coordinate
     */
	public int getX() {
		return posX;
	}

    /**
     * Get the current Y coordinate of the actor
     * @return The current Y coordinate
     */
	public int getY() {
		return posY;
	}

    /**
     * Set the pixel width of the actor
     * @param width
     */
	protected void setWidth(int width) {
		this.width = width;
	}

    /**
     * Get the current pixel width of the actor
     * @return The current pixel width
     */
	public int getWidth() {
		return width;
	}

    /**
     * Set the pixel height of the actor
     * @param height New pixel height
     */
	protected void setHeight(int height) {
		this.height = height;
	}

    /**
     * Get the current pixel height of the actor
     * @return The current pixel height
     */
	public int getHeight() {
		return height;
	}

    /**
     * Set the horizontal velocity of the actor
     * @param vx New horizontal velocity
     */
	public void setVx(int vx) {
		this.vx = vx;
	}

    /**
     * Get the horizontal velocity of the actor
     * @return The current horizontal velocity
     */
	public int getVx() {
		return vx;
	}

    /**
     * Set the vertical velocity of the actor
     * @param vy New vertical velocity
     */
	public void setVy(int vy) {
		this.vy = vy;
	}

    /**
     * Get the vertical velocity of the actor
     * @return The current vertical velocity
     */
	public int getVy() {
		return vy;
	}

    /**
     * Get a rectangle defining the bounds of the actor
     * @return
     */
	public Rectangle getBounds() {
		return new Rectangle(posX,posY,width, height);
	}

    /**
     * Check collisions with an actor
     * @param a
     */
	public void collision(Actor a) {		
	}

    /**
     * Set whether the actor is to be removed
     * @param markedForRemoval
     */
	public void setMarkedForRemoval(boolean markedForRemoval) {
		this.markedForRemoval = markedForRemoval;
	}

    /**
     * Gets whether the actor is marked for removal
     * @return
     */
	public boolean isMarkedForRemoval() {
		return markedForRemoval;
	}

    /**
     * Get point value of the current actor
     * @return
     */
	public int getPointValue() {
		return Actor.POINT_VALUE;
	}
}
