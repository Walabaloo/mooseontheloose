package Actor;

import Stage.Stage;

/**
 * Abstract class for the different enemies
 * collision with an enemy will result in game over
 */
abstract public class Enemy extends Actor {
    public Enemy (Stage canvas) {
        super(canvas);
    }

    /**
     * Method will keep enemies within bounds of the border of the background
     */
    @Override
    public void update() {
        super.update();

        // Mark for removal if enemy strays outside of screen (w/ buffer)
        if (posX > Stage.WIDTH + 100 || (posX + width) < -100)
            setMarkedForRemoval(true);

        if (posY > Stage.HEIGHT + 100 || (posY + height) < -100)
            setMarkedForRemoval(true);
    }
}
