package Actor;

import java.util.Random;

import Stage.Stage;

public class VehicleFactory {
    public static Vehicle createVehicle(Stage canvas) {
        Vehicle vehicle;

        Random random = new Random();

        // True  -> Up
        // False -> Down
        Vehicle.Direction direction = (random.nextBoolean() ? Vehicle.Direction.Up : Vehicle.Direction.Down);
        int modelInt = random.nextInt(4) + 1;
        vehicle = new Car(canvas, random.nextInt(Car.MAX_SPEED) + 1, direction);

        switch (modelInt) {
            case 1:
                vehicle = new Truck(canvas, random.nextInt(Car.MAX_SPEED) + 1, direction);
                break;
            case 2:
                vehicle = new BoxTruck(canvas, random.nextInt(BoxTruck.MAX_SPEED) + 1, direction);
                break;
            case 3:
                vehicle = new Semi(canvas, random.nextInt(Semi.MAX_SPEED) + 1, direction);
                break;
        }

        switch (direction) {
            case Up:
                System.out.println("Up");
                vehicle.setX(762 - vehicle.width/2);
                vehicle.setY(canvas.getHeight());
                break;
            case Down:
                System.out.println("Down");
                vehicle.setX(535 - vehicle.width/2);
                vehicle.setY(0 - vehicle.height);
                break;
        }


        return vehicle;
    }
}
