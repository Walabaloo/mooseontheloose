package Actor;

import Stage.Stage;

/**
 * will create a tree enemy that can be entered into the stage and will cause damage to the player if collided with
 */
public class Tree extends Enemy {
    public Tree(Stage canvas) {
        super(canvas);

        sprites = new String[]{"Tree_1.png"};

        width = 100;
        height = 100;
    }

    @Override
    public void update() {
        super.update();
        updateSpeed();
    }

    public void updateSpeed() {
        vy = stage.getSpeed() / 10;

        posY = posY + vy;
    }
}
