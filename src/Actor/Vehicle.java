package Actor;

import Stage.Stage;

abstract public class Vehicle extends Enemy {
    public enum Direction {
        Up, Down;
    }

    // The max speed of the vehicle model
    public static int MAX_SPEED = 3;

    // Movement directions (negative up and positive down)
    private int mov;

    // Speed on top of the screen scroll
    private int speed;

    /**
     * Create a new car object
     * @param canvas The canvas to draw the car upon
     */
    public Vehicle(Stage canvas, int speed, Direction direction) {
        super(canvas);

        speed = speed > 0 ? speed : 1;
        this.speed = speed <= MAX_SPEED ? speed : MAX_SPEED;

        switch (direction) {
            case Up:
                mov = -1;
                break;
            case Down:
                mov = 1;
                break;
        }
    }

    /**
     * Update the state of the car
     */
    @Override
    public void update() {
        super.update();
        updateSpeed();
    }

    public void updateSpeed() {
        vy = mov * (stage.getSpeed() + speed);

        posY += vy;
    }
}
