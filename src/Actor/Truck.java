package Actor;

import Stage.Stage;

import java.util.Random;

public class Truck extends Vehicle {
    public static int MAX_SPEED = 2;

    public Truck(Stage canvas, int speed, Direction direction) {
        super(canvas, speed, direction);

        int colorNum = new Random().nextInt(2);

        width = 56;
        height = 138;

        switch (direction) {
            case Up:
                if (colorNum == 0) {
                    sprites = new String[]{"CarTruckUp_2.png"};
                }
                else {
                    sprites = new String[]{"CarTruckUp_3.png"};
                }
                break;
            case Down:
                if (colorNum == 0) {
                    sprites = new String[]{"CarTruckDown_2.png"};
                }
                else {
                    sprites = new String[]{"CarTruckDown_3.png"};
                }
                break;
        }
    }
}