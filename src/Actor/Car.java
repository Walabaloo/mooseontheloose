package Actor;

import Stage.Stage;

public class Car extends Vehicle {
    public static int MAX_SPEED = 4;

    public Car(Stage canvas, int speed, Direction direction) {
        super(canvas, speed, direction);

        width = 56;
        height = 108;

        switch (direction) {
            case Up:
                sprites = new String[]{"CarTruckUp_1.png"};
                break;
            case Down:
                sprites = new String[]{"CarTruckDown_1.png"};
        }
    }
}
