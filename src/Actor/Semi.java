package Actor;

import Stage.Stage;

public class Semi extends Vehicle {
    public static int MAX_SPEED = 1;

    public Semi(Stage canvas, int speed, Direction direction) {
        super(canvas, speed, direction);

        width = 64;
        height = 218;

        switch (direction) {
            case Up:
                sprites = new String[]{"CarTruckUp_5.png"};
                break;
            case Down:
                sprites = new String[]{"CarTruckDown_5.png"};
                break;
        }
    }
}
