package Actor;

import Stage.Stage;

/**
 *
 */
public class Invader extends Actor {
	
	private static final int POINT_VALUE = 10;

	private int leftWall = 0;
	private int rightWall = 0;
	private int step = 0;
	private int advanceTime = 1000;
	
	public Invader(Stage stage) {
		super(stage);
		
		if (((int)(Math.random()*10))%2 == 0) 
			sprites = new String[]{"invader1.gif", "invader2.gif"};
		else 
			sprites = new String[]{"invader3.gif", "invader4.gif"};
		
		frame = 0;
		frameSpeed = 50;
		actorSpeed = 100;
		width = 20;
		height = 20;
	}

	public void update() {
		super.update();

		updateXSpeed();
		updateYSpeed();
	}
		
	public void setLeftWall(int leftWall) {
		this.leftWall = leftWall;
	}
	
	public void setRightWall(int rightWall) {
		this.rightWall = rightWall;
	}
	
	private void updateXSpeed() {
		if (time % actorSpeed == 0) {
			posX += getVx();
			if (posX < leftWall || posX > rightWall) setVx(-getVx());
		}
	}
	
	private void updateYSpeed() {
		step++;
		if (step == advanceTime) {
			posY += height;
			step = 0;
		}	

		if (posY == stage.getHeight()) 
			stage.endGame();
	}

	public void collision(Actor a) {
	}
	
	public int getPointValue() {
		return Invader.POINT_VALUE;
	}	
}
