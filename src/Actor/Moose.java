package Actor;

import Stage.Stage;

/**
 *Will add the sprites of the Moose depending on which side the moose is spawned on
 */
public class Moose extends Enemy {
    private int mov;

    public static enum Direction {
        RIGHT, LEFT;
    }

    /**
     * Constructor for the moose class
     * @param canvas
     * @param direction
     */
    public Moose(Stage canvas, Direction direction) {
        super(canvas);

        switch (direction) {
            case LEFT:
                sprites = new String[]{"MooseRight_1.png"};
                mov = 1;
                break;
            case RIGHT:
                sprites = new String[]{"MooseLeft_1.png"};
                mov = -1;
                break;
        }

        width = 120;
        height = 130;
    }

    /**
     * Updates the speed
     */
    public void updateSpeed() {
        vy = stage.getSpeed() / 10;

        posY = posY + vy;
        posX = posX - (mov * vx);
    }

    /**
     * Overridable method for updateSpeed method
     */
    @Override
    public void update() {
        super.update();

        updateSpeed();
    }
}
