package Actor;

import Game.Game;
import Stage.Actionable;
import Stage.Stage;

import java.awt.*;

public class Selection extends Actor {
    private SelectionPrintable text;
    private Actionable action;
    private Graphics graphicsCanvas;

    private Game container;

    private int xPos;
    private int yPos;

    private int fontSize = 48;
    private String fontName = "Arial";
    private Color textColor = Color.WHITE;

    /**
     * Create a new selection
     * @param textString The printable text of the selection
     * @param action The action of the selection
     * @param container The current containing game
     * @param canvas The canvas to draw on
     * @param xPos The X coordinate
     * @param yPos The Y coordinate
     */
    public Selection(String textString, Actionable action, Game container, Stage canvas, int xPos, int yPos) {
        super(canvas);
        this.text = (g, contRef) -> {
            g.setFont(new Font(contRef.getFontName(), Font.BOLD, contRef.getFontSize()));
            g.setColor(contRef.getTextColor());

            g.drawString(textString, contRef.getxPos(), contRef.getyPos());
        };
        this.action = action;
        this.graphicsCanvas = graphicsCanvas;
        this.container = container;
        this.xPos = xPos;
        this.yPos = yPos;
    }

    /**
     * Create a new selection
     * @param textString The printable text of the selection
     * @param action The action of the selection
     * @param container The current containing game
     * @param canvas The canvas to draw on
     * @param xPos The X coordinate
     * @param yPos The Y coordinate
     * @param fontSize The point size of the font
     * @param fontName The name of the font
     * @param textColor The color of the text
     */
    public Selection(String textString, Actionable action, Game container, Stage canvas, int xPos, int yPos, int fontSize, String fontName, Color textColor) {
        super(canvas);
        this.text = (g, contRef) -> {
            g.setFont(new Font(contRef.getFontName(), Font.BOLD, contRef.getFontSize()));
            g.setColor(contRef.getTextColor());

            g.drawString(textString, contRef.getxPos(), contRef.getyPos());
        };
        this.action = action;
        this.graphicsCanvas = graphicsCanvas;
        this.container = container;
        this.xPos = xPos;
        this.yPos = yPos;
        this.fontSize = fontSize;
        this.fontName = fontName;
        this.textColor = textColor;
    }

    /**
     * Create a new selection
     * @param textPrinter The text printer of the selection
     * @param action The action of the selection
     * @param container The current containing game
     * @param canvas The canvas to draw on
     * @param xPos The X coordinate
     * @param yPos The Y coordinate
     */
    public Selection(SelectionPrintable textPrinter, Actionable action, Game container, Stage canvas, int xPos, int yPos) {
        super(canvas);
        this.text = textPrinter;
        this.action = action;
        this.graphicsCanvas = graphicsCanvas;
        this.container = container;
        this.xPos = xPos;
        this.yPos = yPos;
    }

    /**
     * Create a new selection
     * @param textPrinter The text printer of the selection
     * @param action The action of the selection
     * @param container The current containing game
     * @param canvas The canvas to draw on
     * @param xPos The X coordinate
     * @param yPos The Y coordinate
     * @param fontSize The point size of the font
     * @param fontName The name of the font
     * @param textColor The color of the text
     */
    public Selection(SelectionPrintable textPrinter, Actionable action, Game container, Stage canvas, int xPos, int yPos, int fontSize, String fontName, Color textColor) {
        super(canvas);
        this.text = textPrinter;
        this.action = action;
        this.graphicsCanvas = graphicsCanvas;
        this.container = container;
        this.xPos = xPos;
        this.yPos = yPos;
        this.fontSize = fontSize;
        this.fontName = fontName;
        this.textColor = textColor;
    }

    /**
     * Gets texts
     * @return printable text object
     */
    public SelectionPrintable getText() {
        return text;
    }

    /**
     * Sets text
     * @param text
     */
    public void setText(SelectionPrintable text) {
        this.text = text;
    }

    /**
     * Gets x position
     * @return
     */
    public int getxPos() {
        return xPos;
    }

    /**
     * sets x position
     * @param xPos
     */
    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    /**
     * gets y position
     * @return
     */
    public int getyPos() {
        return yPos;
    }

    /**
     * sets y position
     * @param yPos
     */
    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    /**
     * gets the font size
     * @return
     */
    public int getFontSize() {
        return fontSize;
    }

    /**
     * sets the font size
     * @param fontSize
     */
    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    /**
     * gets the font name
     * @return
     */
    public String getFontName() {
        return fontName;
    }

    /**
     * sets the font name
     * @param fontName
     */
    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    /**
     * gets the text colour
     * @return
     */
    public Color getTextColor() {
        return textColor;
    }

    /**
     * sets text colour
     * @param textColor
     */
    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    /**
     * Run the associtaed action
     */
    public void select() {
        action.run();
    }

    /**
     * Paint the selection to the screen
     * @param g The canvas' graphics object
     */
    public void paint(Graphics g) {
        text.paint(g, this);
    }
}
