package Actor;

import java.util.Random;

import Stage.Stage;

/**
 * Will create an enemy at random and from a random direction at a set point on the screen
 * Will also spawn a tree on the side of the road of the game
 */
public class EnemyFactory {
    public static Enemy createEnemy(Stage canvas) {
        Random random = new Random();
        int spawnNumber = random.nextInt(20) + 1;
        Enemy enemy = new Tree(canvas);
        enemy.setX(random.nextInt(360) + (random.nextBoolean() ? 0 : 920 - enemy.getWidth()));

        switch (spawnNumber) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                if (random.nextInt(2) == 0) {
                    enemy = new Moose(canvas, Moose.Direction.RIGHT);
                    enemy.setX(0 - enemy.getWidth());
                }
                else {
                    enemy = new Moose(canvas, Moose.Direction.LEFT);
                    enemy.setX(Stage.WIDTH + enemy.getWidth());
                }
                enemy.setVx(random.nextInt(6) + 1);
                break;
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                enemy = VehicleFactory.createVehicle(canvas);
                break;
            default:
                break;
        }

        enemy.setY(0 - enemy.getHeight());

        return enemy;
    }
}
