package Actor;

import Stage.Stage;
import java.awt.event.KeyEvent;

/**
 * Will allow the player to move across the stage
 */
public class Player extends Actor implements KeyboardControllable {
	
	private boolean up,down,left,right;
	private int score = 0;
	private int speed;

    /**
     * Create a new player
     * @param stage The stage to interact with
     */
	public Player(Stage stage) {
		super(stage);

		sprites = new String[]{"CarTruckUp_1.png"};
		frame = 0;
		frameSpeed = 35;
		actorSpeed = 10;
		width = 56;
		height = 108;
		speed = 50;
		posX = 600;
		posY = Stage.HEIGHT/3;
	}

    /**
     * Update the current player state
     */
	public void update() {
		super.update();		
	}

    /**
     * Update the current player speed
     */
	protected void updateSpeed() {
		vx = 0;
		vy = 0;
		if (down) {
            if ((speed - 5 ) >= 50)
                speed -= 5;
        }

		if (up) {
            if ((speed + 2 ) <= 150)
                speed += 2;
        }

		if (left) {
            vx = -actorSpeed;
        }

		if (right) {
            vx = actorSpeed;
        }
		
		//don't allow scrolling off the edge of the screen		
		if (posX - width/2 > 0 && vx < 0)
			posX += vx;
		else if (posX + width  + (width/2)< Stage.WIDTH && vx > 0)
			posX += vx;
	}

    /**
     * Handle key release events
     * @param e The key event
     */
	public void triggerKeyRelease(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_DOWN:
			down = false;
			break;
		case KeyEvent.VK_UP:
			up = false;
			break;
		case KeyEvent.VK_LEFT:
			left = false;
			break;
		case KeyEvent.VK_RIGHT:
			right = false;
			break;
		}
		updateSpeed();
	}

    /**
     * Handle key press events
     * @param e The key event
     */
	public void triggerKeyPress(KeyEvent e) {
		switch (e.getKeyCode()) {
		///*
		case KeyEvent.VK_UP:
			up = true;
			break;
		//*/
		case KeyEvent.VK_LEFT:
			left = true;
			break;
		case KeyEvent.VK_RIGHT:
			right = true;
			break;
		///*
		case KeyEvent.VK_DOWN:
			down = true;
			break;
		}
		updateSpeed();
	}

    /**
     * Check collision with another actor
     * @param a Another actor
     */
	public void collision(Actor a) {
	    if (a instanceof Enemy)
		    stage.endGame();
	}

    /**
     * Increase the current score
     * @param score Amount to increase score by
     */
	public void updateScore(int score) {
		this.score += score;
	}

    /**
     * Get the current score
     * @return The current score
     */
	public int getScore() {
		return score;
	}

    /**
     * Get the current player speed
     * @return The current speed
     */
	public int getSpeed() {
	    return speed;
    }
}
