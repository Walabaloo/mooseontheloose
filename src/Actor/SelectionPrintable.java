package Actor;

import java.awt.*;

public interface SelectionPrintable {
    public void paint(Graphics g, Selection containerReference);
}
