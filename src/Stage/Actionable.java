package Stage;

public interface Actionable {
    void run();
}
