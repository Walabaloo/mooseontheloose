package Stage;

import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.awt.image.ImageObserver;
import java.util.ArrayList;
import java.util.List;

import Actor.Actor;
import Game.Game;

import javax.swing.*;

abstract public class Stage extends Canvas implements ImageObserver, KeyListener {

	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 1280;
	public static final int HEIGHT = 800;
	public static final int DESIRED_FPS = 60;

	protected long usedTime;
	protected int speed;

	protected Game container;

	protected boolean gameWon = false;
	protected boolean gameOver = false;
    protected BufferStrategy strategy;

    public List<Actor> actors = new ArrayList<Actor>();

    /**
     * Create a new stage
     * @param container The containing Game
     */
	public Stage(Game container) {
	    //Add link to containing game frame
	    this.container = container;
	}

    /**
     * Abstract method meant for drawing one frame of the stage
     * @param g
     */
	abstract public void draw(Graphics g);

    /**
     * Loop the sound with the given name
     * @param name Name of the sound file to play
     */
	public void loopSound(final String name) {
	    container.loopSound(name);
	}

    /**
     * Get the containing game
     * @return The containing game
     */
	public Game getContainer() {
	    return container;
    }

    /**
     * Ends current game session
     */
    public void endGame() {
		gameOver = true;
	}

    /**
     * Gets the current game over status
     * @return Whether the game is over
     */
	public boolean isGameOver() {
		return gameOver;
	}

    /**
     * Update a image
     * @param img The image to update
     * @param infoflags Set info flags
     * @param x The X coordinate
     * @param y The Y coordinate
     * @param width The width
     * @param height The height
     * @return The update status
     */
	public boolean imageUpdate(Image img, int infoflags, int x, int y,
			int width, int height) {
		return false;
	}

    /**
     * Paint the current FPS to the screen
     * @param g
     */
	public void paintFPS(Graphics g, long usedTime) {
		g.setColor(Color.RED);
		if (usedTime > 0)
			g.drawString(String.valueOf(1000/usedTime)+" fps",0,Stage.HEIGHT-50);
		else
			g.drawString("--- fps",0,Stage.HEIGHT-50);
	}

    /**
     * Get the current speed of the stage
     * @return The current speed
     */
    public int getSpeed() {
	    return speed;
    }

    /**
     * Initialise the world state
     */
    public void initWorld() {

    }

    /**
     * Start the stage
     */
    abstract public void start();

    public void game() {

    }
}
