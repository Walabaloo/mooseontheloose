package Stage;

import Actor.Selection;
import Actor.Selector;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

import Game.Game;

abstract public class Screen extends Stage {
    public static int TEXT_X_BUFFER = 125;

    protected List<Selection> selections;
    protected Selector selector;
    protected int defaultSelection;

    private BufferedImage background, backgroundImage;
    private long usedTime;

    /**
     * Create a screen with selectable items
     *
     * @param container        The reference to the containing game frame
     * @param defaultSelection The default selection to select
     */
    public Screen(Game container, String backgroundResourceName, int defaultSelection) {
        super(container);
        selections = new LinkedList<>();

        setBounds(0, 0, Stage.WIDTH, Stage.HEIGHT);
        setBackground(Color.GRAY);

        this.backgroundImage = ResourceLoader.getInstance().getSprite(backgroundResourceName);
        this.background = ResourceLoader.createCompatible(WIDTH, HEIGHT, Transparency.OPAQUE);

        Graphics2D g = (Graphics2D) background.getGraphics();
        g.setPaint(new TexturePaint(backgroundImage, new Rectangle(0, 0, backgroundImage.getWidth(), backgroundImage.getHeight())));
        g.fillRect(0, 0, background.getWidth(), background.getHeight());

        this.defaultSelection = defaultSelection;
    }

    /**
     * Initialises the screens selections, should be called after super() in sub-classes
     */
    abstract protected void initSelections();

    /**
     * Initialises the screens selector, should be called after initSelections() in sub-classes
     */
    protected void initSelector() {
        this.selector = new Selector(this, selections, defaultSelection);
    }

    /**
     * Handle a key typing event
     * @param keyEvent The key event
     */
    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    /**
     * Handle a key pressing event
     * @param keyEvent The key event
     */
    @Override
    public void keyPressed(KeyEvent keyEvent) {
        selector.triggerKeyPress(keyEvent);
    }

    /**
     * Handle a key releasing event
     * @param keyEvent The key event
     */
    @Override
    public void keyReleased(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case KeyEvent.VK_ESCAPE:
                container.previousStage();
            default:
                selector.triggerKeyRelease(keyEvent);
        }
    }

    /**
     * Update the state of the screen
     */
    private void updateScreen() {
        selector.update();
    }

    /**
     * Paint the state of the screen
     * @param g The graphics object to paint to
     */
    private void paintScreen(Graphics g) {
        g.setColor(getBackground());
        g.fillRect(0, 0, getWidth(), getHeight());

        // Draw background
        g.drawImage(backgroundImage, 0, 0, WIDTH, HEIGHT, this);

        for (int i = 0; i < selections.size(); i++) {
            Selection selection = selections.get(i);

            selection.paint(g);
        }

        selector.paint(g);
    }

    /**
     * Draw one frame of the screen
     * @param g
     */
    @Override
    public void draw(Graphics g) {
        updateScreen();
        paintScreen(g);
    }

    /**
     * Initialise the object
     */
    @Override
    public void start() {
        usedTime = 0;
    }
}
