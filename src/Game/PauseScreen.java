package Game;

import Stage.Stage;
import Stage.Screen;
import Actor.Selection;

import java.awt.event.KeyEvent;

/**
 * Will display the Pause Screen
 */
public class PauseScreen extends Screen {
    public PauseScreen(Game container) {
        super(container, "PausedBackground_1.jpg", 0);
        initSelections();
        initSelector();
        requestFocus();
    }

    /**
     * Will give the player the option to resume the game
     */
    @Override
    protected void initSelections() {
        super.selections.add(
            new Selection(
                    "Return to Game",
                    () -> container.previousStage(),
                    container,
                    this,
                    Stage.WIDTH/2 - TEXT_X_BUFFER,
                    Stage.HEIGHT/2
            )
        );

        super.selections.add(
                new Selection(
                        "Exit to Main Menu",
                        () -> container.initState(),
                        container,
                        this,
                        Stage.WIDTH/2 - TEXT_X_BUFFER,
                        Stage.HEIGHT/2 + 45
                )
        );
    }
}
