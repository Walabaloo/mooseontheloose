package Game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.Transparency;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import Stage.Stage;
import Actor.*;

import Stage.InputHandler;
import Stage.ResourceLoader;

import Actor.EnemyFactory;

import javax.swing.*;
import Game.Game;

/**
 * Will create a game called Moose On The Loose that consist of driving a car to avoid
 * various enemies and a Newfoundland-esque locale
 */
public class MooseOnTheLoose extends Stage implements KeyListener {

    private static final long serialVersionUID = 1L;

    private static int SPAWN_TICK_WAIT = 30;
    private int ticksSinceSpawn;

    private Player player;
    private InputHandler keyPressedHandler;
    private InputHandler keyReleasedHandler;

    public long usedTime;//time taken per game step
    private long score;

    private BufferedImage background, backgroundTile; //background cache
    private int backgroundY; //background cache position

    private boolean paused = false;

    /**
     * Create a new Moose on the Loose game
     * @param container The containing game frame
     */
    public MooseOnTheLoose(Game container) {
        //Add the reference to the container
        super(container);

        //init the UI
        setBounds(0,0,Stage.WIDTH,Stage.HEIGHT);
        setBackground(Color.BLACK);

        //create a double buffer
        requestFocus();
        initWorld();

        score = 0;
        ticksSinceSpawn = 0;

        keyPressedHandler = new InputHandler(this, player);
        keyPressedHandler.action = InputHandler.Action.PRESS;
        keyReleasedHandler = new InputHandler(this, player);
        keyReleasedHandler.action = InputHandler.Action.RELSEASE;
    }

    /**
     * Initialise world state
     */
    public void initWorld() {
        actors = new ArrayList<Actor>();
        gameOver = false;
        gameWon = false;
        //add a player
        player = new Player(this);
        player.setX(Stage.WIDTH/2 - player.getWidth()/2);
        player.setY(Stage.HEIGHT - 250);

        //load cached background
        backgroundTile = ResourceLoader.getInstance().getSprite("Highway_1.jpg");
        background = ResourceLoader.createCompatible(
                            WIDTH, HEIGHT+ backgroundTile.getHeight(),
                            Transparency.OPAQUE);
        Graphics2D g = (Graphics2D)background.getGraphics();
        g.setPaint( new TexturePaint( backgroundTile,new Rectangle(0,0,backgroundTile.getWidth(),backgroundTile.getHeight())));
        g.fillRect(0,0,background.getWidth(),background.getHeight());
        backgroundY = backgroundTile.getHeight();
    }

    /**
     * Paint the state of the world to the screen
     * @param g The canvas' graphics object
     */
    public void paintWorld(Graphics g) {
        //init image to background
        g.setColor(getBackground());
        g.fillRect(0, 0, getWidth(), getHeight());

        //load subimage from the background
        g.drawImage( background,0,0,Stage.WIDTH,Stage.HEIGHT,0,backgroundY,Stage.WIDTH,backgroundY+Stage.HEIGHT,this);

        //paint the actors
        for (int i = 0; i < actors.size(); i++) {
            Actor actor = actors.get(i);

            if (container.shouldDrawDebug()) {
                g.setFont(new Font("Arial", Font.BOLD, 10));
                g.setColor(Color.ORANGE);
                g.drawString("Class: " + actor.getClass().toString(), actor.getX(), actor.getY());
                g.drawString("X: " + actor.getX(), actor.getX(), actor.getY() + 10);
                g.drawString("Y: " + actor.getY(), actor.getX(), actor.getY() + 20);
            }

            actor.paint(g);
        }

        player.paint(g);
        paintScore(g);
        paintSpeed(g);
    }

    /**
     * Paint the game over screen
     */
    public void paintGameOver() {
        container.loadStage(new GameOverScreen(container, (int) score / 1000));
    }

    public void paintSpeed(Graphics g) {
        g.setFont(new Font("Arial", Font.BOLD, 20));
        g.setColor(Color.RED);
        g.drawString("Speed: " + player.getSpeed(), Stage.WIDTH - 200, 20);
    }

    /**
     * Paint the score on the screen
     * @param g
     */
    public void paintScore(Graphics g) {
        g.setFont(new Font("Arial",Font.BOLD,20));
        g.setColor(Color.green);
        g.drawString("Time: ",20,20);
        g.setColor(Color.red);
        g.drawString("" + score / 1000, 100, 20);
    }

    public void paint(Graphics g) {}

    /**
     * Update world state
     */
    public void updateWorld() {
        int i = 0;
        speed = player.getSpeed();

        ticksSinceSpawn--;

        if (actors.size() <= 10 && ticksSinceSpawn <= 0) {
            Enemy enemy = EnemyFactory.createEnemy(this);
            actors.add(enemy);
            ticksSinceSpawn = SPAWN_TICK_WAIT;
        }

        while (i < actors.size()) {
            //Check collisions
            Actor actor = actors.get(i);

            actor.update();

            if (actor.isMarkedForRemoval()) {
                System.out.println(actor.getClass());
                //Remove actor
                actors.remove(i);
            }
            else {
                //Continue to next actor index
                i++;
            }
        }

        //Check if anything is colliding with the player
        checkCollision(player);

        System.out.println(
                "Speed:     " + speed + "\n" +
                "Used Time: " + container.getUsedTime() + "\n"
        );
        this.score += (speed * container.getUsedTime());

        //Update the player
        player.update();
    }

    /**
     * Check the collision of the actors on screen
     * @param actor
     */
    private void checkCollision(Actor actor) {

        Rectangle actorBounds = actor.getBounds();
        for (int i = 0; i < actors.size(); i ++) {
            Actor otherActor = actors.get(i);
            if (null == otherActor || actor.equals(otherActor)) continue;
            if (actorBounds.intersects(otherActor.getBounds())) {
                actor.collision(otherActor);
                otherActor.collision(actor);
            }
        }
    }

    /**
     * Draw a frame of the game
     * @param g The canvas graphics object to paint to
     */
    @Override
    public void draw(Graphics g) {
        backgroundY -= player.getSpeed() / 10;
        if (backgroundY < 0)
            backgroundY = backgroundTile.getHeight();

        if (super.gameOver) {
            paintGameOver();
        }

        updateWorld();
        paintWorld(g);
    }

    @Override
    public void start() {
        // Startup sound loop
        loopSound("game_music.mp3");

        usedTime = 0;
    }

    public void keyPressed(KeyEvent e) {
        keyPressedHandler.handleInput(e);
    }

    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_ESCAPE:
                container.loadStage(new PauseScreen(container));
            default:
                keyReleasedHandler.handleInput(e);
                break;
        }
    }

    public void keyTyped(KeyEvent e) {
    }
}