package Game;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.List;
import java.util.LinkedList;
import javax.swing.*;

import Stage.Stage;
import Stage.ResourceLoader;

public class Game extends JFrame implements KeyListener {
    private Stage currentStage;
    private JPanel panel;
    private List<Stage> stages = new LinkedList<>();

    private boolean soundOn = true;
    private boolean displayFPS = false;
    private boolean displayDebug = false;

    private Canvas gameCanvas;
    private BufferStrategy strategy;

    private long usedTime;

    /**
     * Create a new game (frame)
     */
    public Game() {
        // Init the UI
        super("Moose on the Loose");

        // Add container panel
        panel = new JPanel();
        panel.setPreferredSize(new Dimension(Stage.WIDTH, Stage.HEIGHT));
        panel.setLayout(null);

        add(panel);

        // Setup canvas
        gameCanvas = new Canvas();
        gameCanvas.setBackground(Color.WHITE);
        gameCanvas.setBounds(0, 0, Stage.WIDTH, Stage.HEIGHT);
        gameCanvas.addKeyListener(this);

        panel.add(gameCanvas);

        // Setup frame
        setBounds(0, 0, Stage.WIDTH, Stage.HEIGHT);
        setResizable(false);
        setVisible(true);

        gameCanvas.requestFocusInWindow();

        gameCanvas.createBufferStrategy(2);
        strategy = gameCanvas.getBufferStrategy();

        //cleanup on window closing
        addWindowListener(
            new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    ResourceLoader.getInstance().cleanup();
                    System.exit(0);
                }
            }
        );
    }

    /**
     * A convenience method for closing the game
     */
    public void exit() {
        dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    /**
     * KeyListeners to pass the key events to the current (active) stage
     */
    @Override
    public void keyPressed(KeyEvent keyEvent) {
        currentStage.keyPressed(keyEvent);
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case KeyEvent.VK_BACK_QUOTE:
                toggleDrawDebug();
                break;
            case KeyEvent.VK_M:
                toggleSound();
                break;
            case KeyEvent.VK_F:
                toggleDrawFPS();
                break;
            default:
                currentStage.keyReleased(keyEvent);
                break;
        }
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
        currentStage.keyTyped(keyEvent);
    }

    /**
     * Turn drawing FPS on or off
     * @return The state of FPS drawing
     */
    public boolean toggleDrawFPS() {
        displayFPS = !displayFPS;
        return displayFPS;
    }

    /**
     * Get the state of FPS drawing
     * @return The state of FPS drawing
     */
    public boolean shouldDrawFPS() {
        return displayFPS;
    }

    public boolean toggleDrawDebug() {
        displayDebug = !displayDebug;
        return displayDebug;
    }

    public boolean shouldDrawDebug() {
        return displayDebug;
    }

    /**
     * Play a requested sound if sound is on
     * @param name The name of the sound resource
     */
    public void playSound(String name) {
        if (soundOn)
            new Thread(() -> ResourceLoader.getInstance().getSound(name).play()).start();
    }

    /**
     * Loop a requested sound if sound is on
     * @param name The name of the sound resource
     */
    public void loopSound(String name) {
        if (soundOn)
            new Thread(() -> ResourceLoader.getInstance().getSound(name).loop()).start();
    }

    /**
     * Toggle sound on or off
     * @return The state of sound playing
     */
    public boolean toggleSound() {
        if (soundOn) {
            soundOn = false;
            ResourceLoader.getInstance().cleanup();
        } else {
            soundOn = true;
        }

        return soundOn;
    }

    /**
     * Get the state of sound
     * @return The state of sound
     */
    public boolean getSoundStatus() {
        return soundOn;
    }

    /**
     * Set the current stage and add it to the panel
     *
     * @param stage
     */
    private void setStage(Stage stage) {
        currentStage = stage;
    }

    /**
     * Get the current used time
     * @return Current used time
     */
    public long getUsedTime() {
        return usedTime;
    }

    /**
     * Initialise the state of the game, creating a new stage list (stages) with a main menu and loading it
     */
    public void initState() {
        List tempList = new LinkedList<Stage>();
        tempList.add(new MainScreen(this));
//        tempList.add(new MooseOnTheLoose(this));

        stages = tempList;

        setStage(stages.get(0));
    }

    /**
     * Load the previous stage from the stage list (stages) and set it as the current stage, removing the previously
     * current stage from the stage list
     */
    public void previousStage() {
        //Load previous stage from stages
        Stage stage = stages.get(stages.size() - 2);

        setStage(stage);

        //Remove last (previously current) stage from stages
        stages.remove(stages.size() - 1);
    }

    /**
     * Load new stage and append it to the stage list (stages)
     *
     * @param stage The stage to load
     */
    public void loadStage(Stage stage) {
        // Clean up after old stage
        ResourceLoader.getInstance().cleanup();

        // Set new stage
        setStage(stage);
        // Append stage to stage "path"
        stages.add(stage);
        // Start current stage
        currentStage.start();
    }

    public void playAgain() {
        initState();
        loadStage(new MooseOnTheLoose(this));
    }

    /**
     * Start the game and load the needed stages
     */
    public void start() {
        initState();
        usedTime = 0;

        while (isVisible()) {
            long startTime = System.currentTimeMillis();

            Graphics g = strategy.getDrawGraphics();

            currentStage.draw(g);

            strategy.show();

            if (displayFPS)
                currentStage.paintFPS(g, usedTime);

            usedTime = System.currentTimeMillis() - startTime;

            //calculate sleep time
            if (usedTime == 0) usedTime = 1;
            int timeDiff = (int) ((1000 / usedTime) - Stage.DESIRED_FPS);
            if (timeDiff > 0) {
                try {
                    Thread.sleep(timeDiff / 100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Main entrypoint for the game, initialises and starts a new game
     *
     * @param args
     */
    public static void main(String[] args) {
        Game game = new Game();
        game.start();
    }
}
