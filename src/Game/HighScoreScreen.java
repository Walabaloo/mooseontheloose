package Game;

import Actor.Selection;
import Game.Game;
import Stage.Screen;
import Stage.Stage;

import javax.swing.JFrame;
import java.awt.event.KeyEvent;

/**
 * Will display the high score screen if selected
 */
public class HighScoreScreen extends Screen {
    public HighScoreScreen(Game container) {
        super(container, "LeaderboardBackground_1.jpg", 0);
        initSelections();
        initSelector();
    }

    /**
     * Will go to the previous screen if the player hits the back key
     */
    @Override
    protected void initSelections() {
        super.selections.add(
            new Selection(
                "Back",
                () -> container.previousStage(),
                container,
                this,
                Stage.WIDTH/2 - TEXT_X_BUFFER,
                Stage.HEIGHT - 200
            )
        );
    }
}
