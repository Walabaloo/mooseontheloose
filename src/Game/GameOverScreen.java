package Game;

import Actor.Selection;
import Stage.Screen;

import java.awt.*;

/**
 * Will display the game over screen when the player runs out of lives
 */
public class GameOverScreen extends Screen {
    private int score;
    public GameOverScreen(Game container, int score) {
        super(container, "GameoverBackground_1.jpg", 0);
        this.score = score;
        initSelections();
        initSelector();
    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);
        g.setFont(new Font("Arial", Font.BOLD, 48));
        g.setColor(Color.WHITE);
        g.drawString("Score: " + score, WIDTH/2 - TEXT_X_BUFFER, HEIGHT/2 - 20);
    }

    /**
     * Will allow the player to restart the game if "Play Again" is selected
     * Will allow the player to choose the go to the main menu if selected and
     * Option to go to the Leader board if selected
     */
    @Override
    protected void initSelections() {
        super.selections.add(
            new Selection(
                "Play Again",
                    () -> container.playAgain(),
                    container,
                    this,
                    WIDTH/2 - TEXT_X_BUFFER,
                    HEIGHT/2 + 45
            )
        );

        super.selections.add(
            new Selection(
                "Leaderboard",
                () -> container.loadStage(new HighScoreScreen(container)),
                container,
                this,
                WIDTH/2 - TEXT_X_BUFFER,
                HEIGHT/2 + 90
            )
        );

        super.selections.add(
            new Selection(
                "Main Menu",
                () -> container.initState(),
                container,
                this,
                WIDTH/2 - TEXT_X_BUFFER,
                HEIGHT/2 + 135
            )
        );
    }
}
