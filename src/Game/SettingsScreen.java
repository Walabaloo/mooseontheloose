package Game;

import Actor.Selection;
import Stage.Screen;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Will display the Settings screen
 */
public class SettingsScreen extends Screen {
    public SettingsScreen(Game container) {
        super(container, "SettingsBackground_1.jpg", 0);
        initSelections();
        initSelector();
    }

    /**
     * Gives the player options to toggle the sound to be on or off
     * and to be able to view the amount of FPS the game is currently running.
     * The player can also choose to return to the Main menu or if the player is in the pause menu, can resume the game
     */
    @Override
    protected void initSelections() {
        super.selections.add(
                new Selection(
                        (g, contRef) -> {
                            g.setFont(new Font(contRef.getFontName(), Font.BOLD, contRef.getFontSize()));
                            g.setColor(contRef.getTextColor());

                            g.drawString(
                                    "Toggle Sound: " + (container.getSoundStatus() ? "On" : "Off"),
                                    contRef.getxPos(),
                                    contRef.getyPos()
                            );
                        },
                        () -> container.toggleSound(),
                        container,
                        this,
                        WIDTH/2 - TEXT_X_BUFFER,
                        HEIGHT/2
                )
        );

        super.selections.add(
            new Selection(
                (g, contRef) -> {
                    g.setFont(new Font(contRef.getFontName(), Font.BOLD, contRef.getFontSize()));
                    g.setColor(contRef.getTextColor());

                    g.drawString(
                            "Toggle FPS Display: " + (container.shouldDrawFPS() ? "On" : "Off"),
                            contRef.getxPos(),
                            contRef.getyPos()
                    );
                },
                () -> container.toggleDrawFPS(),
                container,
                this,
                WIDTH/2 - TEXT_X_BUFFER,
                HEIGHT/2 + 45
            )
        );

        super.selections.add(
            new Selection(
                "Back",
                () -> container.previousStage(),
                container,
                this,
                WIDTH/2 - TEXT_X_BUFFER,
                HEIGHT/2 + 90
            )
        );
    }
}
