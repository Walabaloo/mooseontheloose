package Game;

import Stage.Stage;
import Stage.Screen;
import Actor.Selection;

import java.awt.event.KeyEvent;

public class MainScreen extends Screen {
    /**
     * Create a new main screen
     * @param container
     */
    public MainScreen(Game container) {
        super(container, "MainBackground_1.jpg", 0);
        initSelections();
        initSelector();
    }

    /**
     * Gives the player different options on the Main Menu that will be selected from a selector
     * If play is selected the game will start
     * If options is selected the player will go to the option menu
     * If Leaderboard is selected the player ill go to the Leaderboard menu
     * If exit is selected the player will exit the game
     */
    @Override
    protected void initSelections() {
        super.selections.add(
            new Selection(
                "Play",
                () -> container.loadStage(new MooseOnTheLoose(container)),
                container,
                this,
                Stage.WIDTH/2 - TEXT_X_BUFFER,
                Stage.HEIGHT/2
            )
        );

        super.selections.add(
            new Selection(
                "Options",
                () -> container.loadStage(new SettingsScreen(container)),
                container,
                this,
                Stage.WIDTH/2 - TEXT_X_BUFFER,
                Stage.HEIGHT/2 + 45
            )
        );

        super.selections.add(
            new Selection(
                "Leaderboard",
                () -> container.loadStage(new HighScoreScreen(container)),
                container,
                this,
                Stage.WIDTH/2 - TEXT_X_BUFFER,
                Stage.HEIGHT/2 + 90
            )
        );

        super.selections.add(
            new Selection(
                    "Exit",
                    () -> container.exit(),
                    container,
                    this,
                    Stage.WIDTH/2 - TEXT_X_BUFFER,
                    Stage.HEIGHT/2 + 135
            )
        );
    }

    /**
     * If the player hits the "Q" key or the escape key they will exit the game
     * @param keyEvent The key event
     */
    @Override
    public void keyReleased(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case KeyEvent.VK_Q:
            case KeyEvent.VK_ESCAPE:
                container.exit();
                break;
            default:
                super.keyReleased(keyEvent);
                break;
        }
    }
}
